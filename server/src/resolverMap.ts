import { IResolvers } from 'graphql-tools';

const resolverMap: IResolvers = {
  Query: {
    newest: (_source, _args, { dataSources }) => {
      return dataSources.thingiverseApi.getNewest();
    },
    popular: (_source, _args, { dataSources }) => {
      return dataSources.thingiverseApi.getPopular();
    },
    featured: (_source, _args, { dataSources }) => {
      return dataSources.thingiverseApi.getFeatured();
    },
    thingDetail: (_source, { id }, { dataSources }) => {
      return dataSources.thingiverseApi.getThingById(id);
    }
  },
};

export default resolverMap;
