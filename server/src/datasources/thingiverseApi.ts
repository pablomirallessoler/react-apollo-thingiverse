import { RESTDataSource, RequestOptions } from 'apollo-datasource-rest';

export class ThingiverseApi extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://api.thingiverse.com/';
  }

  willSendRequest(request: RequestOptions) {
    request.params.set('access_token', this.context.authorization);
  }

  getNewest() {
      return this.get('newest');
  }

  getPopular() {
    return this.get('popular');
  }

  getFeatured() {
    return this.get('featured');
  }

  getThingById(id: number) {
    return this.get(`things/${id}`);
  }
}
