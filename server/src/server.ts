import express from 'express';
import compression from 'compression';
import cors from 'cors';
import schema from './schema';
import depthLimit from 'graphql-depth-limit';
import { ApolloServer } from 'apollo-server-express';
import { createServer } from 'http';
import { ThingiverseApi } from './datasources/thingiverseApi';
import { parse } from 'query-string';
import bodyParser from 'body-parser';
import axios from 'axios';
const path = require('path');

const app = express();
app.use('*', cors());
app.use(compression());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../../client/dist')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../../client/dist/index.html'));
});

app.post('*', (req, res, next) => {
  next();
});

app.post('/login', (req, res) => {
  const { body: { code } } = req;
  const params = {
    client_id: 'a411498dcffb63e86da4',
    client_secret: '263d3388d314e594673e8a75d247c4f4',
    code: code
  };

  axios.request({
    method: 'POST',
    url: 'https://www.thingiverse.com/login/oauth/access_token',
    params: params
  }).then(response => res.json(parse(response.data)))
  .catch(err => res.status(403).send('Authentication error'));

});

const server = new ApolloServer({
  schema,
  validationRules: [depthLimit(7)],
  context: ({ req }) => {
    const { headers: { authorization } } = req;
    return { authorization };
  },
  dataSources: () => {
      return { thingiverseApi: new ThingiverseApi() };
  }
});
server.applyMiddleware({ app, path: '/graphql' }); 

const httpServer = createServer(app);

httpServer.listen(
  { port: 3000 },
  (): void => console.log(`\n🚀      App is now running on http://localhost:3000`));