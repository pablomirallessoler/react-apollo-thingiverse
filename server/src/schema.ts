import 'graphql-import-node';
import * as typeDefs from './schema/schema.graphql';
import resolvers from './resolverMap';
import { makeExecutableSchema } from 'graphql-tools';
import { GraphQLSchema } from 'graphql';

const schema: GraphQLSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

export default schema;