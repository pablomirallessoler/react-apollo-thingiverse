# Node, React, Apollo, TS

## Requirements

1. node
2. git


## Steps to Setup

**1. Clone the application**

```bash
git clone https://bitbucket.org/pablomirallessoler/react-apollo-thingiverse.git
```

**2. Run the app using npm**

```bash
cd react-apollo-thingiverse
cd server
npm run build:start
```

App will run on <http://localhost:3000>