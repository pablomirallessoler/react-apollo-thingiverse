import { Thing } from './interfaces';

type ThingQuery = { query: ThingQueryOption };

type ThingQueryOption = 'featured' | 'newest' | 'popular';

type ThingResponse = {
  [K in ThingQueryOption]:  Thing[];
};

export { ThingQueryOption, ThingResponse, ThingQuery };