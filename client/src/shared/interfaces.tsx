interface Creator {
    name: string
    first_name: string
    last_name: string
}

interface Thing {
    id: number
    name: string
    creator: Creator
    thumbnail: string
}

export { Thing, Creator };
