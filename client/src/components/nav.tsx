import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { StyleSheet, css } from 'aphrodite';

interface NavLinks { to: string, description: string };

const navLinks: NavLinks[] = [
  {
    to: '/newest',
    description: 'Newest'
  },
  {
    to: '/popular',
    description: 'Popular'
  },
  {
    to: '/featured',
    description: 'Featured'
  }
];

interface NavProps { auth: boolean, logout: Function }

class Nav extends React.Component<NavProps> {

    constructor(props: NavProps) {
      super(props);
      this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout() {
      this.props.logout();
    }

    render() {
      return (
        <div className={css(styles.topNav)}>
          {
            navLinks.map( navLink => 
              <NavLink key={navLink.description} exact to={navLink.to} className={css(styles.navItem)} activeClassName={css(styles.selected)}>
                {navLink.description}
              </NavLink>)
          }
          { this.props.auth ? <a href="javascript:;" className={css(styles.navItem, styles.logoutItem)} onClick={this.handleLogout}>Logout</a> : '' }
        </div>
      );
    }
}

const styles = StyleSheet.create({
    selected: {
        backgroundColor: 'red'
    },

    topNav: {
      overflow: 'hidden',
      backgroundColor: '#333'
    },

    navItem: {
      float: 'left',
      display: 'block',
      color: '#f2f2f2',
      textAlign: 'center',
      padding: '14px 16px',
      textDecoration: 'none',
      ':hover': {
        backgroundColor: '#ddd',
        color: 'black'
      }
    },

    logoutItem: {
      float: "right"
    }
  });
  
  
export default Nav;