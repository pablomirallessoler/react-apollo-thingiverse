import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Thing as ThingInterface } from '../shared/interfaces';
import { StyleSheet, css } from 'aphrodite';

class Thing extends React.Component<RouteComponentProps & ThingInterface> {

  constructor(props:any) {
    super(props);
    this.handleOpenDetail = this.handleOpenDetail.bind(this);
  }

  handleOpenDetail() {
    this.props.history.push(`/detail/${this.props.id}`); 
  }

  render() {
      return (
        <li onClick={this.handleOpenDetail} className={css(styles.cardItem)}>
          <div className={css(styles.card)}>
            <img className={css(styles.cardImage)} src={this.props.thumbnail}></img>
            <div className={css(styles.cardContent)}>
              <div className={css(styles.cardTitle)}>{this.props.name}</div>
              <p className={css(styles.cardText)}>
                <strong>Creator:</strong>
                <br/>
                {this.props.creator.name}
                <br/><br/>
                <strong>Full Name:</strong>
                <br/>
                {this.props.creator.first_name} {this.props.creator.last_name}
              </p>
            </div>
          </div>
        </li>
      );
    }
  }

  const styles = StyleSheet.create({
    cardItem: {
      display: 'flex',
      padding: '1rem',
      '@media(min-width: 30rem)': {
        width: '50%'
      },
      '@media(min-width: 40rem)': {
        width: '33.3333%'
      },
      '@media(min-width: 56rem)': {
        width: '20%'
      }
    },
    card: {
      backgroundColor: 'white',
      borderRadius: '0.25rem',
      boxShadow: '0 20px 40px -14px rgba(0,0,0,0.25)',
      display: 'flex',
      flexDirection: 'column',
      overflow: 'hidden',
      minWidth: '100%',
      color: '#696969',
      ':hover': {
        cursor: 'pointer',
        boxShadow: '0 20px 40px -14px rgba(0,0,0,0.50)',
        color: 'red'
      }
    },
    cardText: {
      flex: '1 1 auto',
      fontSize: '0.875rem',
      lineHeight: '1.5',
      marginBottom: '1.25rem',
      color: '#696969',
    },
    cardContent: {
      display: 'flex',
      flex: '1 1 auto',
      flexDirection: 'column',
      padding: '1rem'
    },
    cardTitle: {
      fontSize: '1.25rem',
      fontWeight: 300,
      letterSpacing: '2px',
      textTransform: 'uppercase'
    },
    cardImage: {
      display: 'block',
      height: 'auto',
      width: '100%' ,
      borderTopLeftRadius: '0.25rem',
      borderTopRightRadius: '0.25rem',
      overflow: 'hidden',
      position: 'relative',
      ':before': {
        content: '""',
        display: 'block',
        paddingTop: '56.25%' // 16:9 aspect ratio
      },
      '@media(min-width: 40rem)': {
        ':before': {
          paddingTop: '66.6%' // 3:2 aspect ratio
        }
      }
    }
  });

export default Thing;
