import * as React from 'react';
import * as queryString from 'query-string';
import { RouteComponentProps } from "react-router";
import { StyleSheet, css } from 'aphrodite';

interface LoginProps { setAccessToken: Function }

class Login extends React.Component<RouteComponentProps & LoginProps>  {

  componentDidMount() {
    const { code }  = queryString.parse(this.props.location.search);
    if (code) {
      this.props.history.push('/login');    
      fetch('http://localhost:3000/login', {
        method: 'POST',
        body: JSON.stringify({ code }),
        headers: {
          'Content-Type': 'application/json'
        }
      }).then((response) => response.json()
      ).then(data => {
        this.props.setAccessToken({ accessToken: data.access_token });
      })  
    }
  }

  render() {
      return (
      <div className={css(styles.login)}>
        <a href="https://www.thingiverse.com/login/oauth/authorize?client_id=a411498dcffb63e86da4&redirect_uri=http://localhost:3000/login&response_type=code">
          Login in Thingiverse...
        </a>
      </div>
      );
    }
  }

  const styles = StyleSheet.create({
    login: {
      padding: '2em',
      textAlign: 'center',
      fontSize: '2rem'
    }
  });

export default Login;