import * as React from 'react';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { RouteComponentProps } from 'react-router-dom';
import { StyleSheet, css } from 'aphrodite';

class ThingDetail extends React.Component<RouteComponentProps<{ id: string }>> {

  constructor(props:any) {
    super(props);
    this.handleGoBack = this.handleGoBack.bind(this);
  }

  handleGoBack() {
    this.props.history.goBack(); 
  }

  getListQuery = gql`
  {
    thingDetail(id: ${this.props.match.params.id}) {
        id,
        name,
        details,
        description_html,
        thumbnail
    }
  }
`;

  render() {
      return (
          <Query<any> query={ this.getListQuery }>
          {({ loading, error, data }) => {
            if (loading) return <div className={css(styles.message)}>Loading...</div>;
            if (error) return <div className={css(styles.message)}>Error! {error.message}</div>;
            return (
                    <div className={css(styles.container)}>
                      <span className={css(styles.title)}>
                          {data.thingDetail.name}
                          <button className={css(styles.backButton)} onClick={this.handleGoBack}>Go Back</button>
                      </span>
                      <div className={css(styles.imageContainer)}>
                          <img className={css(styles.image)} src={data.thingDetail.thumbnail} />
                      </div>
                      <div dangerouslySetInnerHTML={{__html: data.thingDetail.description_html}}></div>
                      <div dangerouslySetInnerHTML={{__html: data.thingDetail.details}}></div>
                    </div>
            );
          }}
          </Query>
      );
    }
  }

  const styles = StyleSheet.create({
    message: {
        padding: '2em',
        textAlign: 'center',
        fontSize: '2rem'
    },
    container: {
        width: '80%',
        margin: '2rem auto',
        backgroundColor: 'white',
        boxShadow: '0 20px 40px -14px rgba(0,0,0,0.25)',
        padding: '2rem'
    },
    backButton: {
        float: 'right',
        color: 'rgb(255, 255, 255)',
        textAlign: 'center',
        padding: '14px 16px',
        backgroundColor: 'red',
        cursor: 'pointer'
    },
    title: {
      fontSize: '1.25rem',
      fontWeight: 300,
      letterSpacing: '2px',
      textTransform: 'uppercase'
    },
    imageContainer: {
      maxWidth: '150px',
      minWidth: '150px',
      paddingTop: '20px'
    },
    image: {
      minWidth: '100%',
      maxWidth: '100%'
    }
  });

export default ThingDetail;