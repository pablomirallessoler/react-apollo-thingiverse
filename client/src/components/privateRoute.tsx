import * as React from 'react';
import { Route, Redirect } from "react-router-dom";

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('accessToken')
            ? <Component {...props} query={ rest.query }/>
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
  );