import * as React from 'react';
import { PrivateRoute } from './privateRoute';
import { Route } from "react-router-dom";
import { RouterProps, Redirect } from "react-router";
import QueryContainer from '../containers/query';
import ThingDetail from './thingDetail';
import Nav from './nav';
import Login from './login';

interface AppState { auth: boolean }

class App extends React.Component<RouterProps, AppState> {

  constructor(props: RouterProps) {
    super(props);
    this.handleAccessToken = this.handleAccessToken.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  state = {
    auth: false
  };

  componentDidMount() {
    this.setState({ auth: !!localStorage.getItem('accessToken') });
  }

  handleAccessToken({ accessToken }) {
    localStorage.setItem('accessToken', accessToken);
    this.setState({ auth: !!accessToken });
    this.props.history.push('/newest');
  }

  handleLogout() {
    localStorage.removeItem('accessToken');
    this.setState({ auth: false });
  }

  render() {
    return (
      <div>
        <Nav logout={ this.handleLogout } auth={ this.state.auth }/>
        <div>
          <Route exact path="/" render={()=> <Redirect to="/newest"/>} />
          <Route path="/login" render={(props) => <Login {...props} setAccessToken={ this.handleAccessToken } />} />
          <PrivateRoute exact path="/newest" component={QueryContainer} query="newest" /> 
          <PrivateRoute exact path="/popular" component={QueryContainer} query="popular" />
          <PrivateRoute exact path="/featured" component={QueryContainer} query="featured" />
          <PrivateRoute exact path="/detail/:id" component={ThingDetail} />
        </div>
      </div>
    );
  }
}

export default App;
