import * as React from 'react';
import Thing from '../components/thing'
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { ThingResponse, ThingQuery } from '../shared/types';
import { StyleSheet, css } from 'aphrodite';
import { RouteComponentProps } from 'react-router-dom'; 

class QueryContainer extends React.Component<RouteComponentProps & ThingQuery> {

    getListQuery = gql`
      {
        ${this.props.query} {
          id
          name
          thumbnail
          creator {
            id
            name
            first_name
            last_name
          }
        }
      }
    `;

    render() {
      return (
      <ul className={css(styles.cards)}>
          <Query<ThingResponse> query={ this.getListQuery }>
          {({ loading, error, data }) => {
            if (loading) return <div className={css(styles.message)}>Loading...</div>;
            if (error) return <div className={css(styles.message)}>Error! {error.message}</div>;
            return (
              data[this.props.query].map(element => (
                <Thing {...this.props} key={element.id} { ...element } />
              ))
            );
          }}
          </Query>
        </ul>);
    }
}

const styles = StyleSheet.create({
  cards: {
    display: 'flex',
    flexWrap: 'wrap',
    listStyle: 'none',
    margin: '0',
    padding: '0'
  },
  message: {
    padding: '2em',
    textAlign: 'center',
    fontSize: '2rem'
  }
});

export default QueryContainer;
